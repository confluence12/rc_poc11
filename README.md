
## **How to setup the project:**

  

-   Make sure you have java 8 or 11 (recommended) installed.
    
-   Install maven from [here](https://mirrors.estointernet.in/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.zip) (optional)
    
-   Clone the below repo

  

	<code>  git clone https://gitlab.com/confluence12/rc_poc11 -b irm </code>


	

 
  

## How to run the project:

###   If you have intellij/sts/eclipse installed:
    

-   Import the project
    
-   Add this project as an maven project
    
-   Run the project. `main class - (ConfluenceMigratorApplication.java)`
    

 ### If you have maven installed (externally) :
    

-   use cmd/terminal and cd  to the root of this project
    
-   Execute this command:  `mvn spring-boot:run`
    

###   If you don’t have IDE/maven installed:
    

-   use cmd/terminal and cd to the root of this project
    
-   Execute this command: `./mvnw spring-boot:run`
    

###   If you have docker installed:
    

-   use cmd/terminal and cd to the root of this project
    
-   Build the image with this command: `docker build -t conf-migrator .`
    
-   Run the container with the following command:
        <code>docker run -p 8000:8000 -p 8888:8888 --name migrator-app conf-migrator</code>

  

## REST Endpoints:

### Call the endpoint in the following order

### Use any API client (Postman/Chrome)

  


> **GET** /process/pages

    
(To get the details about individual pages and create an hierarchy)

>  **GET** /create/gdrive/hierarchy
    

(To create the empty schema of the whole confluence space in gdrive)

>   **GET** async/start/job
    

(This will start processing the files and upload it to google drive asynchronously )

>  **GET** /process/space/{space-key}
    

(This will process only a single space) provide the spaceKey as a pathvariable

  
  

## Note:

### For the initial execution of the application, an URL will be printed in the console. Copy and paste it in your browser. It will ask for Google OAUTH access for accessing your google drive in order to store the exported files.

### While giving access click advanced option on the google access page and select proceed anyway (unsafe app) option.
