package com.ironmountain.confluence.migrate;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication
@EnableBatchProcessing
@EnableAsync
@ComponentScan("com.ironmountain")
public class ConfluenceMigratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfluenceMigratorApplication.class, args);

	}

}
