package com.ironmountain.confluence.migrate.dao;

import com.ironmountain.confluence.migrate.model.JiraIssue;
import org.springframework.data.repository.CrudRepository;

public interface JiraIssueDao extends CrudRepository<JiraIssue, String> {
}
