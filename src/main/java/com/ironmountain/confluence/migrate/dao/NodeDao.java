package com.ironmountain.confluence.migrate.dao;

import com.ironmountain.confluence.migrate.model.Node;
import com.ironmountain.confluence.migrate.model.NodeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NodeDao extends JpaRepository<Node, NodeId> {
}
