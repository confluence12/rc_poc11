package com.ironmountain.confluence.migrate.dao;

import com.ironmountain.confluence.migrate.model.GDrivePage;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


public interface GDrivePageDao extends CrudRepository<GDrivePage, String> {
    GDrivePage findTopByPageSlugContaining(String htmlPageName);
    Optional<GDrivePage> findGDrivePageByPageSlug(String pageSlug);
    Optional<GDrivePage> findGDrivePageByPageId(String pageId);
    List<GDrivePage> findBygDriveIdIsNull();
}
