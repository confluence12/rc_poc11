package com.ironmountain.confluence.migrate.dao;
import com.ironmountain.confluence.migrate.model.Page;
import com.ironmountain.confluence.migrate.model.PageId;
import com.ironmountain.confluence.migrate.model.ReportView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface PageDao extends CrudRepository <Page,PageId>{
    @Query("select p from Page p where p.spaceKey=:sKey")
    List<Page> findPageBySpaceKey(@Param("sKey") String spaceKey);

    Optional<Page> findPageByPageId(String pageId);

    @Query("select p from Page p where p.spaceKey=?1 and p.status not in ?2")
    List<Page> findBySpaceKeyAndStatusNot(String spaceKey, List<String> status);

    @Query("select count(p) from Page p where p.spaceKey=?1 and p.status in ?2")
    int findBySpaceKeyAndStatus(String spaceKey, List<String> status);

    @Query("select p.pageId as pageId, p.pageName as pageName, p.status as status, g.gDriveId as driveId from Page p inner join GDrivePage g ON p.pageId = g.pageId where p.spaceKey = ?1")
    List<ReportView> getPageStatusAndGdriveId(String spaceKey);

    @Query("select count(p) from Page p where p.spaceKey = ?1")
    int findTotalPageInSpace(String spaceKey);
}
