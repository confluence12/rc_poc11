package com.ironmountain.confluence.migrate.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ironmountain.confluence.migrate.model.Node;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NodeDto {
    String title;
    String id;
    String spaceKey;
    Node parent;
    Set<Node> children;

    public NodeDto(String title, String id, String spaceKey, Node parent, Set<Node> children) {
        this.title = title;
        this.id = id;
        this.spaceKey = spaceKey;
        this.parent = parent;
        this.children = children;
    }

    public NodeDto() {
    }
}
