package com.ironmountain.confluence.migrate.model;

public class MigrationReport {
    private String page;
    private String status;

    public MigrationReport(String page, String status) {
        this.page = page;
        this.status = status;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
