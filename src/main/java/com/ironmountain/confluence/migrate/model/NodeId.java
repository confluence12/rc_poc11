package com.ironmountain.confluence.migrate.model;


import java.io.Serializable;
import java.util.Objects;

public class NodeId implements Serializable {
    String title;
    String id;



    public NodeId() {
    }

    public NodeId(String title, String id) {
        this.title = title;
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "NodeId{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NodeId)) return false;
        NodeId nodeId = (NodeId) o;
        return title.equals(nodeId.title) && id.equals(nodeId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, id);
    }
}
