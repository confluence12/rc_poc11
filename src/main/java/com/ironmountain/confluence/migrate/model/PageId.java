package com.ironmountain.confluence.migrate.model;

import java.io.Serializable;
import java.util.Objects;

public class PageId implements Serializable {
    String spaceKey;
    String pageId;

    public PageId(String spaceKey, String pageId) {
        this.spaceKey = spaceKey;
        this.pageId = pageId;
    }

    public PageId() {
    }

    public String getSpaceKey() {
        return spaceKey;
    }

    public String getPageId() {
        return pageId;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }



    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    @Override
    public String toString() {
        return "PageId{" +
                "spaceKey='" + spaceKey + '\'' +
                ", pageId='" + pageId + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PageId)) return false;
        PageId pageId1 = (PageId) o;
        return spaceKey.equals(pageId1.spaceKey) && pageId.equals(pageId1.pageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(spaceKey, pageId);
    }
}
