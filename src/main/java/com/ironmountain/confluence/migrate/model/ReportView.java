package com.ironmountain.confluence.migrate.model;

public interface ReportView {
    String getPageId();
    String getPageName();
    String getStatus();
    String getDriveId();
}
