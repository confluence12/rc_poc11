package com.ironmountain.confluence.migrate.model;


public class Space {
    String spaceKey;
    //String projectId;


    public Space() {
    }

    public Space(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    public String getSpaceKey() {
        return spaceKey;
    }

    public void setSpaceKey(String spaceKey) {
        this.spaceKey = spaceKey;
    }

    @Override
    public String toString() {
        return "Space{" +
                "spaceKey='" + spaceKey + '\'' +
                '}';
    }
}
