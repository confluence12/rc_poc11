package com.ironmountain.confluence.migrate.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class RunWithRetries {
    public static Logger log = LogManager.getLogger(RunWithRetries.class);

    public static <T> T run(CheckedFunction<T> throwableMethod, int maxAttempt, String errorMessage){
        List<Exception> exceptions = new ArrayList<>();
        for(int attempted=0; attempted<maxAttempt; attempted++){
            try{
                if(attempted>0){
                    Thread.sleep(4000);
                }
                return throwableMethod.get();
            }catch (Exception e){
                exceptions.add(e);
            }
        }
        log.error(errorMessage);
        printAggregatedException(exceptions);
        throw new RuntimeException(exceptions.get(0));
    }

    public static void printAggregatedException(List<Exception> exceptions){
        for(Exception exception : exceptions){
            log.error(exception.getMessage());
        }
    }
}
