package com.ironmountain.confluence.migrate.util;

@FunctionalInterface
public interface CheckedFunction<T> {
   T get() throws Exception;
}