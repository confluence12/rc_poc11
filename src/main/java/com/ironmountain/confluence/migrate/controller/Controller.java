package com.ironmountain.confluence.migrate.controller;


import com.ironmountain.confluence.migrate.model.Page;
import com.ironmountain.confluence.migrate.service.*;
import com.ironmountain.confluence.migrate.service.asyncInitialtor.AsyncReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;


@RestController
public class Controller {

    @Autowired
    Initiator initiator;

    @Autowired
    PageService pageService;

    @Autowired
    AsyncReader reader;

    @Autowired
    GDriveStructureCreator gDriveStructureCreator;

    @Autowired
    SummaryReportGenerator summaryReportGenerator;

    @Autowired
    GDriveService gDriveService;


    public static final Logger log = LogManager.getLogger(Controller.class);


    @GetMapping(value="/start/processing/")
    String beginConfluenceProcessing() throws Exception {
         initiator.processSpace(null, false);
         return "Successfully parsed files";
    }

    @GetMapping(value = "/process/pages")
    String buildPages() throws Exception {
        initiator.processSpace(null, true);
        return "db populated with slugs";
    }

    @GetMapping(value = "/process/page/{spaceKey}")
    String buildSingleSpacePages(@PathVariable("spaceKey")String spaceKey) throws Exception {
        initiator.processSpace(spaceKey, true);
        return "db populated with slugs";
    }

    @GetMapping(value = "/get/pages")
    List<Page> getPages(){
        return pageService.getAllPages();
    }

    @RequestMapping("async/start/job")
    public String handleAsync() throws Exception {
        reader.initializeData(null, true, false);

        return "Batch job has been invoked";
    }

    @RequestMapping("async/start/job/{spaceKey}")
    public String handleAsyncSingleSpace(@PathVariable("spaceKey")String spaceKey) throws Exception {
        reader.initializeData(spaceKey, true, false);

        return "Batch job has been invoked";
    }

    @GetMapping(value="/create/gdrive/hierarchy")
    public String createGdriveHierarchy(){
        return gDriveStructureCreator.createHierarchy(null, false);
    }

    @GetMapping(value="/create/gdrive/hierarchy/{spaceKey}")
    public String createGdriveHierarchySingleSpace(@PathVariable("spaceKey")String spaceKey){
        return gDriveStructureCreator.createHierarchy(spaceKey, false);
    }

    @GetMapping(value = "/process")
    public String startWholeProcessing(@RequestParam("spaceKey")String spaceKey, @RequestParam("uploadGdrive") boolean uploadToGdrive ) throws Exception {
        initiator.startWholeProcessing(spaceKey, uploadToGdrive);
        return "processed the given space";
    }

    @GetMapping(value = "/retry/{spaceKey}")
    public String retryProcessing(@PathVariable("spaceKey")String spaceKey){
        initiator.retryProcessing(spaceKey);
        return "retrying "+spaceKey;
    }

    @GetMapping(value = "/space/{spaceKey}/report")
    public Map getMigrationReport(@PathVariable("spaceKey")String spaceKey){
        return pageService.getMigrationStatusReport(spaceKey);
    }

    @GetMapping(value = "/report")
    public String generateReport(@RequestParam String spaceKey) throws IOException {
        summaryReportGenerator.generateReport(spaceKey);
        return "report generated for "+spaceKey;
    }

    @GetMapping(value = "/retry/gdrive/{spaceKey}")
    public String retryCreateEmptyPage(@PathVariable("spaceKey")String spaceKey){
        gDriveService.retryCreateEmptyPage(spaceKey);
        return "retrying creation of empty gdrive page";
    }

}
