package com.ironmountain.confluence.migrate.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
@Async
public class RequestHelper {
    @Autowired
    @Qualifier("restTemplate")
    RestTemplate restTemplate;
    public static Logger log = LogManager.getLogger(RequestHelper.class);

    public CompletableFuture<JSONArray> getResponse(String url) {
        log.info("firing request " + url);
        JSONObject resObj = new JSONObject(restTemplate.getForObject(url, String.class));
        return CompletableFuture.completedFuture(resObj.getJSONObject("page").getJSONArray("results"));

    }
}
