package com.ironmountain.confluence.migrate.service;

import com.ironmountain.confluence.migrate.model.Attachment;
import com.ironmountain.confluence.migrate.model.Upload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This service class holds several methods for further processing the
 * converted markdown file
 *
 * @author Nandakumar12
 */
@Service
public class ParseHtml {

    @Autowired
    PageService pageService;

    @Value("${confluence.domain}")
    String confluenceDomain;

    @Autowired
    @Qualifier("restTemplate")
    RestTemplate restTemplate;

    @Autowired
    LinkParser linkParser;

    @Autowired
    @Lazy
    ConfluenceDownload confluenceDownload;

    @Autowired
    TableParser tableParser;

    @Value("${confluence.slugs.confluence-attachment-slug}")
    private String CONFLUENCE_ATTACHMENT_SLUG;


    public static final Logger log = LogManager.getLogger(ParseHtml.class);


    /**
     * This method holds the main logic for parsing the markdown file
     * it fixes broken links, parses the attachments and userprofile urls
     *
     * @param filePath           This the path of the markdown file which needs to be processed
     * @param pageId             This is the confluence page id for the provided markdown file
     * @param gdriveHtmlFileName This is the name of the wiki page in gitlab, where the markdown contents
     *                           will be uploaded into
     * @return nothing
     */
    Upload parse(Path filePath, String pageId, String pageSpaceKey, String gdriveHtmlFileName) throws InterruptedException {
        log.info("parsing the html content of page {}", pageId);
        StringBuilder data = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(String.valueOf(filePath.toAbsolutePath())))) {
            String line;
            while ((line = br.readLine()) != null) {
                data.append(line).append("\n");
            }
        } catch (IOException e) {
            log.error(e);
        }
        String currentPageSlug = pageService.getPage(pageSpaceKey, pageId).get().getHtmlPageSlug();
        Document document = Jsoup.parse(data.toString(), confluenceDomain);
        log.info("extracting and replacing url of page {} in html", pageId);
        Elements links = document.select("a[href]");
        Elements medias = document.select("img[src]");
        String htmlName = filePath.getFileName().toString().replaceFirst("(.md)$", "");
        JSONArray attachmentDetails = new JSONObject(restTemplate.getForObject(confluenceDomain + String.format(CONFLUENCE_ATTACHMENT_SLUG, pageId), String.class))
                .getJSONObject("children")
                .getJSONObject("attachment")
                .getJSONArray("results");

        List<String> parsedLinks = new ArrayList<>();

        Elements iconImgTags = document.select("img.icon");
        for (Element imgTag : iconImgTags) {
            imgTag.remove();
        }

        for (Element link : links) {
            try {
                Pair<String, String> newUrlData = linkParser.replaceLink(link, filePath, currentPageSlug, attachmentDetails, htmlName, pageId);
                link.attr("href", newUrlData.getFirst());
                parsedLinks.add(newUrlData.getSecond());

            } catch (Exception e) {
                log.error(" cant able to replace link ERR {} PAGE-ID {} LINK- {}", e.getMessage(), pageId, link);
                log.error(e.toString());
            }
        }
        for (Element media : medias) {
            try {
                if (media.toString().contains("class=\"expand-control-image\"")) {
                    media.remove();
                } else {
                    Pair<String, String> newUrlData = linkParser.replaceLink(media, filePath, currentPageSlug, attachmentDetails, htmlName, pageId);
                    media.removeAttr("src");
                    media.attr("href", newUrlData.getFirst());
                    media.text(newUrlData.getSecond());
                    parsedLinks.add(newUrlData.getSecond());
                }
            } catch (Exception e) {
                log.error(" cant able to replace link ERR- {} PAGE-ID {} LINK- {}", e.getMessage(), pageId, media);
                log.error(e.toString());
            }
        }
        medias.tagName("a");

        List<Attachment> attachmentList = confluenceDownload.retrieveAttachments(pageId, htmlName);
        if (!attachmentList.isEmpty()) {
            Element attachmentHtml = new Element("div");
            Element attachmentSeparator = new Element("p");
            Element attachmentHeader = new Element("h3");
            attachmentSeparator.text("------------------------------------------------------------");
            attachmentHeader.text("Attachments");
            attachmentHtml.appendChild(attachmentSeparator);
            attachmentHtml.appendChild(attachmentHeader);

            for (Attachment attachment : attachmentList) {
                if (!parsedLinks.contains(attachment.getName())) {
                    Element url = new Element("a");
                    url.text(attachment.getName());
                    url.attr("href", attachment.getUrl());
                    attachmentHtml.appendChild(url);
                    attachmentHtml.appendChild(new Element("br"));
                }

            }
            document.appendChild(attachmentHtml);
        }

        Elements htmlTables = document.select("table")
                .stream()
                .filter(table -> rowSize(table)>3)
                .collect(Elements::new, ArrayList::add, ArrayList::addAll);

        List<CompletableFuture<String>> tableFutures = new ArrayList<>();

        int tableCounter = 0;
        for (Element table : htmlTables) {
                String tableName = "Table " + ++tableCounter;
                try {
                    tableFutures.add(tableParser.parse(table, tableName, htmlName, pageId));
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        CompletableFuture<Void> allTableFutures = CompletableFuture.allOf(tableFutures.toArray(new CompletableFuture[tableFutures.size()]));
        CompletableFuture<List<String>> listCompletableFuture = allTableFutures.thenApply(unused -> tableFutures.stream().map(CompletableFuture::join).collect(Collectors.toList()));
        List<String> gdriveUrls = null;
        try {
            gdriveUrls = listCompletableFuture.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        tableCounter = 0;

        for (Element table : htmlTables) {
            String tableName = "Table " + ++tableCounter;
            String gdriveUrl = gdriveUrls.remove(0);
            Element url = new Element("a");
            url.text(tableName);
            url.attr("href", gdriveUrl);
            table.appendChild(url);
            Element lineBreak = new Element("br");
            table.appendChild(lineBreak);
        }


        try (BufferedWriter br = new BufferedWriter(new FileWriter(filePath.toAbsolutePath().toString()))) {
            br.write(document.toString());
        } catch (IOException e) {
            log.error(e);
        }
        Upload upload = new Upload(pageSpaceKey, pageId, filePath.toAbsolutePath().toString(), gdriveHtmlFileName);
        log.info("data to be uploaded in gdrive {}", upload);

        return upload;


    }

    int rowSize(Element table){
        Element htmlTableHeader = table.select("tr").get(0);
        Elements headerTd = htmlTableHeader.select("td,th");
        return headerTd.size();
    }


}
