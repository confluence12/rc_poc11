package com.ironmountain.confluence.migrate.service;

import com.ironmountain.confluence.migrate.dao.PageDao;
import com.ironmountain.confluence.migrate.model.Page;
import com.ironmountain.confluence.migrate.model.ReportView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Service
public class SummaryReportGenerator {

    public static final String GDRIVE_DOCUMENT_URL = "https://docs.google.com/document/d/";

    private static final String[] columns = {"Page id", "Name", "Status","Google Drive link"};

    @Autowired
    PageDao pageDao;

    public static Logger log = LogManager.getLogger(StructureBuilder.class);

    public void generateReport(String spaceKey) throws IOException {

        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet(spaceKey);

        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        List<ReportView> reportViewList = pageDao.getPageStatusAndGdriveId(spaceKey);
        log.info("Generating summary report for {}",spaceKey);

        int rowNum = 1;
        for(ReportView reportView: reportViewList) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0)
                    .setCellValue(reportView.getPageId());
            row.createCell(1)
                    .setCellValue(reportView.getPageName());
            row.createCell(2)
                    .setCellValue(reportView.getStatus());

            Cell cell = row.createCell(3);
            XSSFHyperlink link = (XSSFHyperlink)createHelper.createHyperlink(HyperlinkType.URL);
            link.setAddress(GDRIVE_DOCUMENT_URL+reportView.getDriveId());
            cell.setCellValue("link");
            cell.setHyperlink(link);

        }

        int summaryRowNum = 0;
        Sheet sheet2 = workbook.createSheet("SUMMARY");
        Row row0 = sheet2.createRow(summaryRowNum++);

        int totalPage = pageDao.findTotalPageInSpace(spaceKey);
        row0.createCell(0)
                .setCellValue("Total page");
        row0.createCell(1)
                .setCellValue(totalPage);

        Row row1 = sheet2.createRow(summaryRowNum++);

        int successPage = pageDao.findBySpaceKeyAndStatus(spaceKey, Collections.singletonList(Page.status.DONE.name()));
        row1.createCell(0).
                setCellValue("Successfully uploaded page");
        row1.createCell(1)
                .setCellValue(successPage);

        Row row2 = sheet2.createRow(summaryRowNum++);
        row2.createCell(0).
                setCellValue("Failed page");
        row2.createCell(1)
                .setCellValue(totalPage - successPage);


        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        for(int i = 0; i < 2; i++) {
            sheet2.autoSizeColumn(i);
        }

        FileOutputStream fileOut = new FileOutputStream(spaceKey+".xlsx");
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
        log.info("Report generated for {}",spaceKey);

    }
}
