package com.ironmountain.confluence.migrate.service;

import com.google.common.collect.Lists;
import com.ironmountain.confluence.migrate.dao.GDrivePageDao;
import com.ironmountain.confluence.migrate.model.GDrivePage;
import com.ironmountain.confluence.migrate.model.Node;
import com.ironmountain.confluence.migrate.model.Page;
import com.ironmountain.confluence.migrate.util.RunWithRetries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URLEncoder;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * This service class holds several methods for creating the hierarchy structure of
 * the confluence space, so that the same structure will be maintained in the
 * gitlab too
 *
 * @author  Nandakumar12
 */
@Service
public class StructureBuilder {

    @Autowired
    @Qualifier("restTemplate")
    RestTemplate restTemplate;

    @Value("${confluence.domain}")
    String confluenceDomain;

    @Value("${confluence.slugs.confluence-child-page-slug}")
    String confluenceChildPage;

    @Value("${confluence.slugs.confluence-space-slug}")
    String confluenceSpaceDetail;

    @Value("${confluence.slugs.confluence-metadeta-slug}")
    String confluenceMetaData;

    @Value("${confluence.slugs.confluence-orphan-page-slug}")
    String confluenceOrphanPage;

    @Value("${confluence.slugs.confluence-ancestor-slug}")
    String confluenceAncestorSlug;

    @Value("${confluence.slugs.confluence-homepage-hierarchy-info}")
    String confluenceHomepageInfo;

    @Autowired
    PageService pageService;

    @Autowired
    NodeService nodeService;

    @Autowired
    GDriveService gDriveService;

    @Autowired
    GDrivePageDao gDrivePageDao;


    @Autowired
    @Qualifier("asyncExecutor")
    ThreadPoolTaskExecutor executor;


    @Value("${gDrive.max-depth-limit}")
    Integer depthLimit;

    int maxThreads = (int) (Runtime.getRuntime().availableProcessors() / (1-0.3));

    public static Logger log = LogManager.getLogger(StructureBuilder.class);

    /**
     * This method will build a General Tree structure from the json data returned
     * by confluence api, so that we can able to maintain the same hierarchy structure among
     * the gitlab wikis
     *
     * @param spaceKey This the confluence space identifier for which we need to build the structure
     * @return nothing
     *
     */

    public Node buildStructure(String spaceKey, boolean updateDb) throws Exception {
        String spaceDetail = restTemplate.getForObject(confluenceDomain+confluenceSpaceDetail+spaceKey, String.class );
        String homePageUrl = new JSONObject(spaceDetail).getJSONObject("_expandable").getString("homepage");

        int index = homePageUrl.lastIndexOf("/");
        String homePageId = homePageUrl.substring(index+1);
        log.info("homepage id {} ",homePageId);
        // [;\/:*?""<>|&']
        Map<String, String> urlVars = new HashMap<>();
        urlVars.put("spaceKey",spaceKey);
        urlVars.put("homepageId",homePageId);
        int totalPages = new JSONObject(restTemplate.getForObject(confluenceDomain+confluenceHomepageInfo,String.class, urlVars)).getInt("totalSize")+1;
        log.info("total no of pages {}",totalPages);

        String homePageName = new JSONObject(restTemplate.getForObject(confluenceDomain+confluenceMetaData+"/"+homePageId,String.class)).getString("title");
        Node root= nodeService.createNode(new Node(homePageName.replaceAll("[;\\/\\\\:*?\"<>|&']", "_"),homePageId,spaceKey, null,new HashSet<>()));

        String rootSpaceFolderId = RunWithRetries.run(()->gDriveService.createFolder(null, spaceKey, true),3,"can't able to create root folder "+spaceKey);
        processInitialChildPage(root, spaceKey, homePageId, rootSpaceFolderId, updateDb, totalPages, false);
        processOrphanPages(spaceKey, homePageId, rootSpaceFolderId);
        return root;

    }

    void processOrphanPages(String spaceKey, String homepageId, String rootSpaceFolderId){
        Map<String, String> metaInfoUrlVars = new HashMap<>();
        metaInfoUrlVars.put("spaceKey",spaceKey);
        metaInfoUrlVars.put("homepageId", homepageId);
        metaInfoUrlVars.put("pageStart","0");
        int currentPage = 0 ;
        log.info(metaInfoUrlVars);
        int totalOrphanPages =  new JSONObject(restTemplate.getForObject(confluenceDomain+confluenceOrphanPage , String.class, metaInfoUrlVars )).getInt("totalSize");;
        log.info("whole orphan size {}",totalOrphanPages);

        while (currentPage < totalOrphanPages){
            Map<String, String> urlVars = new HashMap<>();
            urlVars.put("spaceKey",spaceKey);
            urlVars.put("homepageId", homepageId);
            urlVars.put("pageStart",Integer.toString(currentPage));
            JSONObject orphanPageResponse = new JSONObject(restTemplate.getForObject(confluenceDomain+confluenceOrphanPage , String.class, urlVars ));

            for(Object o: orphanPageResponse.getJSONArray("results")){
                JSONObject page = (JSONObject) o;
                JSONObject content = page.getJSONObject("content");
                String pageId = content.getString("id");
                String pageTitle = content.getString("title");
                Map<String, String> orphanPageUrlVars = new HashMap<>();
                orphanPageUrlVars.put("spaceKey",spaceKey);
                orphanPageUrlVars.put("homepageId",pageId);
                int totalPages = new JSONObject(restTemplate.getForObject(confluenceDomain+confluenceHomepageInfo,String.class, orphanPageUrlVars)).getInt("totalSize")+1;
                log.info("checking orphan {}",pageTitle);
                if(!hasAncestors(pageId, spaceKey)){
                    log.info("current orphan root {} : size {}", pageTitle,totalPages);
                    Node root= nodeService.createNode(new Node(pageTitle.replaceAll("[;\\/\\\\:*?\"<>|&']", "_"), pageId, spaceKey, null, new HashSet<>()));
                    try {
                        processInitialChildPage(root, spaceKey, pageId, rootSpaceFolderId, true, totalPages, true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
            currentPage+=500;
        }


    }

    void processInitialChildPage(Node root, String spaceKey, String pageId, String rootSpaceFolderId, boolean updateDb, int totalPage, boolean isOrphan) throws IOException {
        String childPageUrl = confluenceDomain+confluenceChildPage;
        childPageUrl = String.format(childPageUrl, root.getId());
        List<Page> result = new ArrayList<>();

        result.add(new Page(root.getSpaceKey(), pageId, root.getTitle(), root.getTitle(), Page.status.CREATED_HIERARCHY.name()));
        String fileName = root.getTitle()+".html";

        String parentFolderId = RunWithRetries.run(()->gDriveService.createFolder(rootSpaceFolderId, root.getTitle(), false),3,"can't able to create parent folder "+fileName);
        String fileId = gDriveService.createEmptyFileWrapper(parentFolderId, fileName);
        gDrivePageDao.save(new GDrivePage(spaceKey+"/"+root.getTitle()+"/"+fileName, fileId, pageId, parentFolderId));
        AtomicInteger counter = new AtomicInteger(1);
        processCalls(root, childPageUrl, result, root.getTitle(), parentFolderId, totalPage, counter);
        if(updateDb){

            for (List<Page> tmpList: Lists.partition(result, 500)) {
                pageService.createPages(tmpList);
            }

            log.info("tree has been built");
            log.info("creating page slugs");
        }
        log.info("populated database with pageslugs");

    }


    boolean hasAncestors(String pageId, String spaceKey){
        JSONObject pageDetails = new JSONObject(restTemplate.getForObject(confluenceDomain+String.format(confluenceAncestorSlug, pageId), String.class));
        JSONArray ancestors = pageDetails.getJSONArray("ancestors");
        return ancestors.length() != 0;


    }

    List<Supplier> formHierarchy(Node parent, String nextPage, List<Page> result, String slug, String parentFoldedGdriveId, int currentDepth, String safeParent, boolean initialRun, AtomicInteger counter){
        JSONObject childPages = new JSONObject(restTemplate.getForObject(nextPage, String.class));
        JSONArray childPageArr = childPages.getJSONArray("results");
        JSONObject nextLink = childPages.getJSONObject("_links");

        Node child = null;
        String childPageUrl = null;
        String tmpSlug = null;
        String parentId = null;

        List<Supplier> pendingCalls = new ArrayList<>();
        log.info("the current depth {}",currentDepth);
        log.info("active threads  {}",executor.getActiveCount());

        if(currentDepth>=depthLimit){
            parentFoldedGdriveId = safeParent;
            currentDepth = 0;
        }

        for(Object o: childPageArr){
            JSONObject page = (JSONObject) o;
            String pageId = page.getString("id");
            String pageTitle = page.getString("title").replaceAll("[;\\/\\\\:*?\"<>|&']", "_");

            String fileName = pageTitle+".html";
            parentId = null;
            String finalParentFoldedGdriveId1 = parentFoldedGdriveId;
            parentId = RunWithRetries.run(()->gDriveService.createFolder(finalParentFoldedGdriveId1, pageTitle, false),3,"can't able to create folder in gdrive "+pageTitle);
            String fileId = gDriveService.createEmptyFileWrapper(parentId, fileName);
            tmpSlug = slug+"/"+pageTitle;
            gDrivePageDao.save(new GDrivePage(tmpSlug+"/"+fileName, fileId, pageId, parentId ));
            log.info("counter value {}",counter.incrementAndGet());;

            child = nodeService.createNode(new Node(pageTitle.replaceAll("[;\\/\\\\:*?\"<>|&']", "_"),pageId, parent.getSpaceKey(), null,new HashSet<>()));
            nodeService.setChild(parent, child);
            childPageUrl = confluenceDomain+confluenceChildPage;
            childPageUrl = String.format(childPageUrl, child.getId());
            result.add(new Page(parent.getSpaceKey(), pageId, pageTitle, tmpSlug, Page.status.CREATED_HIERARCHY.name()));
            //formHierarchy(child, childPageUrl, result, tmpSlug , parentId);
            String finalChildPageUrl = childPageUrl;
            String finalParentId = parentId;
            Node finalChild1 = child;
            String finalTmpSlug1 = tmpSlug;
            int finalCurrentDepth1 = currentDepth;
            if(currentDepth==0 && initialRun){
                safeParent = parentId;
            }
            String finalSafeParent = safeParent;
            pendingCalls.add(() -> formHierarchy(finalChild1, finalChildPageUrl, result, finalTmpSlug1, finalParentId, finalCurrentDepth1 +1, finalSafeParent, false, counter));

        }

        String base = nextLink.getString("base");
        String nextPageId;
        try {
            nextPageId = nextLink.getString("next");
        } catch (org.json.JSONException jsonException) {
            nextPageId = null;
        }

        if (nextPageId != null) {
            nextPage = base + nextPageId;
            String finalNextPage = nextPage;
            String finalParentFoldedGdriveId = parentFoldedGdriveId;
            int finalCurrentDepth = currentDepth;
            String finalSafeParent1 = safeParent;
            pendingCalls.add(()->formHierarchy(parent, finalNextPage, result, slug, finalParentFoldedGdriveId, finalCurrentDepth +1, finalSafeParent1, false, counter));
        }

        return pendingCalls;

    }

    void processCalls(Node root, String childPageUrl, List<Page> result,String slug,  String rootFolderId, int totalPageCount, AtomicInteger counter) {
        List<Supplier> callsToBeProcessed = new ArrayList<>(formHierarchy(root, childPageUrl, result, slug, rootFolderId, 0, null, true, counter));
        while (!callsToBeProcessed.isEmpty()) {
            callsToBeProcessed.addAll((Collection<? extends Supplier>) callsToBeProcessed.remove(0).get());
        }
    }

        List<Future<Void>> resultFutures = new ArrayList<>();
        for(Consumer consumer: consumers){
            resultFutures.add((Future<Void>) executor.submit(consumer));
        }

//        BlockingQueue<Supplier> callsToBeProcessed = new LinkedBlockingQueue<>(formHierarchy(root, childPageUrl, result, slug, rootFolderId, 0, null, true, counter));
//
//        List<Consumer> consumers = new ArrayList<>();
//        for(int i=0;i<maxThreads;i++){
//            consumers.add(new Consumer(callsToBeProcessed, counter, totalPageCount));
//        }
//
//        List<Future<Void>> resultFutures = new ArrayList<>();
//        for(Consumer consumer: consumers){
//            resultFutures.add((Future<Void>) executor.submit(consumer));
//        }
//
//        for(Future<Void> future: resultFutures){
//            try {
//                future.get();
//            } catch (InterruptedException | ExecutionException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }
//
//    static class Consumer implements Runnable {
//        private final BlockingQueue<Supplier> sharedQueue;
//        private final AtomicInteger count;
//        private final int totalCount;
//
//        public Consumer (BlockingQueue<Supplier> sharedQueue, AtomicInteger count, int totalCount) {
//            this.sharedQueue = sharedQueue;
//            this.count = count;
//            this.totalCount = totalCount;
//        }
//        @Override
//        public void run() {
//            while(count.get() < totalCount){
//                try {
//                    log.info("polling");
//                    Supplier pendingCall = sharedQueue.poll(15, TimeUnit.SECONDS);
//                    if(pendingCall!=null) {
//                        sharedQueue.addAll((Collection<? extends Supplier>) pendingCall.get());
//                    }
//                } catch (Exception err) {
//                    err.printStackTrace();
//                }
//            }
//            log.info("exiting {}",Thread.currentThread().getName());
//        }
//    }


}
