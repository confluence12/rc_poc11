package com.ironmountain.confluence.migrate.service;

import com.ironmountain.confluence.migrate.dao.GDrivePageDao;
import com.ironmountain.confluence.migrate.util.RunWithRetries;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class TableParser {

    @Autowired
    GDriveService gDriveService;

    @Autowired
    GDrivePageDao drivePageDao;

    private static final String MD_ATTACHMENT_DIRECTORY = "./files/attachments/";

    @Async
    CompletableFuture<String> parse(Element htmlTable, String fileName, String htmlName, String pageId) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();
        Sheet sheet = workbook.createSheet("1");

        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.RED.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        Element htmlTableHeader = htmlTable.select("tr").get(0);
        Elements headerTd = htmlTableHeader.select("td,th");
        List<String> tableHeader = new ArrayList<>();
        for (Element td:headerTd){
            tableHeader.add(td.select("p").text());
            td.remove();
        }
        for(int i=0;i<tableHeader.size();i++){
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(tableHeader.get(i));
            cell.setCellStyle(headerCellStyle);
        }

        Elements htmlTableRows = htmlTable.select("tr");
        for(int rownum = 1; rownum<htmlTableRows.size();rownum++){
            Row row = sheet.createRow(rownum);
            Elements rowTd = htmlTableRows.get(rownum).select("td,th");
            for(int colnum = 0; colnum < rowTd.size(); colnum++){
                Element cellElement = rowTd.get(colnum);
                if(containsAnchorTag(cellElement)){
                    Cell cell = row.createCell(colnum);
                    XSSFHyperlink link = (XSSFHyperlink)createHelper.createHyperlink(HyperlinkType.URL);
                    Pair<String, String> newCellValue = parseAnchorTag(cellElement);
                    cell.setCellValue(newCellValue.getFirst());
                    link.setAddress(newCellValue.getSecond());
                    cell.setHyperlink(link);
                }else{
                    row.createCell(colnum)
                            .setCellValue(rowTd.get(colnum).text());
                }
                rowTd.get(colnum).remove();
            }
        }
        for(int i = 0; i < tableHeader.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        String absPath = MD_ATTACHMENT_DIRECTORY + htmlName;
        Files.createDirectories(Paths.get(absPath));
        FileOutputStream fileOut = new FileOutputStream(absPath+"/"+fileName+".xlsx");
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();

        String parentId = drivePageDao.findGDrivePageByPageId(pageId).get().getParentFolderId();
        String attachmentsFolderId = RunWithRetries.run(()->gDriveService.findOrCreateAttachmentFolder(parentId, "Attachments"),3, "Cant able to create attachment folder in GDrive");
        String gdriveUrl = RunWithRetries.run(()->gDriveService.uploadFile(absPath+"/"+fileName+".xlsx", attachmentsFolderId ).get(),3 ,"Can't able to upload file to google drive");
        return CompletableFuture.completedFuture(gdriveUrl);

    }

    boolean containsAnchorTag(Element element){
        Elements anchorTag = element.select("a");
        return anchorTag.size() != 0;
    }

    Pair<String, String> parseAnchorTag(Element element){
        String text = element.text();
        String url = element.attr("href");
        return Pair.of(text, url);
    }

}
