import base64

import requests
from requests.auth import HTTPBasicAuth
import re
import uuid
import json
import io

from io import BytesIO
#from StringIO import StringIO


##############################################################################
## General

# *False* if Jira / GitLab is using self-signed certificates, otherwhise *True*
VERIFY_SSL_CERTIFICATE = True

##############################################################################
#f = open("config.json","r")
#content = f.read()
#print(content)
#data = json.loads(content)
#print(data)
with open("config.json") as f:
    data = json.load(f)   
#for k, v in data['Jira_account'].items():
        #print(v)
JIRA_URL = data['Jira_url']
JIRA_USERNAME = data['Jira_username']
JIRA_PASSWORD = data['Jira_password']
#print("jira accnt: :", JIRA_ACCOUNT)
JIRA_PROJECT = data['Jira_project']
print("Jira project: ", JIRA_PROJECT)
#JQL = data['Jql']
JIRA_NEED_TO_BE_DONE_FIELD = data['Jira_need_to_be_done_field']
JIRA_CRITICALITY_OF_BUGS_FIELD = data['Jira_criticality_of_bugs_field']
JIRA_REQUIREMENTS_NEED_TO_BE_DONE_FIELD = data['Jira_requirements_need_to_be_done_field']
GITLAB_URL = data['Gitlab_url']
GITLAB_TOKEN = data['Gitlab_token']
GITLAB_GROUP_ID = data['Gitlab_group_id']
GITLAB_PROJECT_ID = data['Gitlab_project_id']
f.close()

JQL = 'project=%s++&maxResults=100' % JIRA_PROJECT
#JQL = 'maxResults=100'
#JQL = 'project+%3D+RC+AND+maxResults=100+%3D+'
print("jql: ", JQL)
##############################################################################
## Import configuration

# Add a comment with the link to the Jira issue
ADD_A_LINK = True
# Add an Epic to the GitLab issue
ADD_EPIC = True
# Add a milestone/sprint to the GitLab issue
ADD_SPRINT = True

# Convert Jira issue types to Gitlab labels
# Note: If a Jira issue type isn't in the map, the issue will be skipped
ISSUE_TYPES_MAP = {
    'Bug': 'Bug::high',
    'Improvement': 'Enhancement',
    'Spike': 'Spike',
    'Epic':'Epic',
    'Story': 'Story',
    'Task': 'Task',
    'Subtask': 'Sub-task'
}

# Convert Jira story points to Gitlab issue weight
STORY_POINTS_MAP = {
    1.0: 1,
    2.0: 2,
    3.0: 3,
    5.0: 5,
    8.0: 8,
    13.0: 13,
    21.0: 21,
}

JIRA_GITLAB_ISSUE_MAPPER = {}
JIRA_GIRLAB_EPIC_MAPPER = {}
JIRA_GITLAB_iSSUE_ID_MAPPER = {}

##############################################################################


# GET request
def jira_get_request(endpoint):
  auth_header = get_authorization_header()
  #b64Val = base64.b64encode(JIRA_ACCOUNT.encode("utf-8")).decode()
  #b64Val = [x.encode('utf-8') for x in JIRA_ACCOUNT]
  #print(type(b64Val))
  #b64Val = [x.decode() for x in JIRA_ACCOUNT]
  #print(type(b64Val))
  print("base64 val: ", auth_header)
  response = requests.get(
    JIRA_URL + endpoint,
    #auth=HTTPBasicAuth('csg_gitlab_migration_poc_team.groups@ironmountain.com','Donotel@1'),
     verify=VERIFY_SSL_CERTIFICATE,
     headers={'Content-Type': 'application/json', 'Authorization': auth_header}
  )

  if response.status_code != 200:
    print("error code: ", response.status_code) 
    raise Exception("Unable to read data from %s!" % JIRA_PROJECT)
    

  return response.json()


def get_authorization_header():
    usrPass = "%s:%s" % (JIRA_USERNAME, JIRA_PASSWORD)
    b64Val = base64.b64encode(usrPass.encode("utf-8")).decode()
    return "Basic %s" % b64Val


def jira_downlaod_attachment(fileUrl):
    auth_header = get_authorization_header()
    response = requests.get(
		fileUrl,
		verify=VERIFY_SSL_CERTIFICATE,
        headers={'Authorization': auth_header}
		)

    return BytesIO(response.content)


##############################################################################

# GET request
def gl_get_request(endpoint):
  response = requests.get(
    GITLAB_URL + endpoint,
    headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
    verify=VERIFY_SSL_CERTIFICATE
  )

  if response.status_code != 200:
    raise Exception("Unable to read data from %s!" % GITLAB_PROJECT_ID)

  return response.json()

  # POST request
def gl_post_request(endpoint, data):
  response = requests.post(
    GITLAB_URL + endpoint,
    headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
    verify=VERIFY_SSL_CERTIFICATE,
    data=data
  )

  if response.status_code != 201:
    raise Exception("Unable to write data to %s!" % GITLAB_PROJECT_ID)

  return response.json()

# PUT request
def gl_put_request(endpoint, data):
  response = requests.put(
    GITLAB_URL + endpoint,
    headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
    verify=VERIFY_SSL_CERTIFICATE,
    data=data
  )

  if response.status_code != 200:
    raise Exception("Unable to change data from %s!" % GITLAB_PROJECT_ID)

  return response.json()

def gl_post_attachment(endpoint, data):
  response = requests.post(
    GITLAB_URL + endpoint,
    headers={'PRIVATE-TOKEN': GITLAB_TOKEN},
    verify=VERIFY_SSL_CERTIFICATE,
    files=data
  )

  if response.status_code != 201:
    raise Exception("Unable to write data to %s!" % GITLAB_PROJECT_ID)

  return response.json()

##############################################################################

# Get Jira issues
jira_issues = jira_get_request('/search?jql=' + JQL)

# Get GitLab members of the project
gl_members = gl_get_request('/projects/%s/members' % GITLAB_PROJECT_ID)

for issue in jira_issues['issues']:
  jira_key = issue['key']
  jira_issue_type = issue['fields']['issuetype']['name']

  # Map Jira Issue Tyoe to GitLab issue type as label
  gl_issue_type = ''
  if jira_issue_type not in ISSUE_TYPES_MAP:
    print("Unknown issue type detected. Jira issue %s skipped" % jira_key)
    continue
  else:
    gl_issue_type = ISSUE_TYPES_MAP[jira_issue_type]

  print("Start import of Jira issue %s" % jira_key)
  print("Jira Issue Type %s " % jira_issue_type)
  print("GitLab issue Type: %s" %gl_issue_type)

  jira_title = issue['fields']['summary']
  jira_description = issue['fields']['description']
  jira_issue_status = issue['fields']['status']['statusCategory']['name']
  jira_reporter = issue['fields']['reporter']['displayName']
  jira_created_at = issue['fields']['created']

  print("Jira Title: " + jira_title)
  # print("Description: %s" %jira_description)
  print("Status: %s"  %jira_issue_status)

  # Get Jira assignee
  jira_assignee = ''
  if issue['fields']['assignee'] is not None and issue['fields']['assignee']['displayName'] is not None:
    jira_assignee = issue['fields']['assignee']['displayName']

  # Get GitLab assignee
  gl_assignee_id = 0
  for member in gl_members:
    if member['name'] == jira_assignee:
      gl_assignee_id = member['id']
      break

  # Print Assignee Details
  print("Issue Assignee: %s" %jira_assignee)
  print("Issue Assignee GitLab ID: %s" %gl_assignee_id)

  # Add GitLab issue type as label
  gl_labels = []
  gl_labels.append(gl_issue_type)

  #Get lables for the specific issue
  jira_labels = jira_get_request('/issue/%s?fields=labels' %jira_key)

  if jira_labels['fields']['labels']:
  	print("Issue key %s have labels available" %jira_key)

  	for label in jira_labels['fields']['labels']:
  		gl_labels.append(label)
  		print("Jira labels: %s" %label)

  # Add "In Progress" to labels
  if jira_issue_status == "In Progress":
    gl_labels.append(jira_issue_type)


  # Add Jira ticket to labels
  gl_labels.append('jira-import::' + jira_key)

  # Add Custom fields as Scope Labels to the issue
  if JIRA_NEED_TO_BE_DONE_FIELD in issue['fields'] and issue['fields'][JIRA_NEED_TO_BE_DONE_FIELD] is not None:
    print("Adding Custom Filed %s Value" % JIRA_NEED_TO_BE_DONE_FIELD)
    customfiled_property_list = issue['fields'][JIRA_NEED_TO_BE_DONE_FIELD]
    customfiled_property_dict = customfiled_property_list[0]   
    gl_labels.append('need_to_be_done::'+ customfiled_property_dict[u'value'])  


  if JIRA_REQUIREMENTS_NEED_TO_BE_DONE_FIELD in issue['fields'] and issue['fields'][JIRA_REQUIREMENTS_NEED_TO_BE_DONE_FIELD] is not None:
    print("Adding Custom Filed %s Value" % JIRA_REQUIREMENTS_NEED_TO_BE_DONE_FIELD)
    customfiled_property_dict = issue['fields'][JIRA_REQUIREMENTS_NEED_TO_BE_DONE_FIELD]  
    gl_labels.append('requirements_need_to_be_done::'+ customfiled_property_dict['value'])  

  if JIRA_CRITICALITY_OF_BUGS_FIELD in issue['fields'] and issue['fields'][JIRA_CRITICALITY_OF_BUGS_FIELD] is not None:
    print("Adding Custom Filed %s Value" % JIRA_CRITICALITY_OF_BUGS_FIELD)
    customfiled_property_list = issue['fields'][JIRA_CRITICALITY_OF_BUGS_FIELD]
    customfiled_property_dict = customfiled_property_list[0]   
    gl_labels.append('criticality_of_bug::'+ customfiled_property_dict[u'value'])

  #Check if it is an Epic and if so Create a new Epic
  if jira_issue_type == 'Epic':
  	print("Epic found posting to Epics")
  	response = gl_post_request('/groups/%s/epics' %GITLAB_GROUP_ID,{
  		'id':jira_key,
  		'title':jira_title,
  		'description':jira_description,
  		'created_at':jira_created_at,
  		'labels':",".join(gl_labels)
  		})
  	#Map GitLab Epic With Jira Epic
  	JIRA_GIRLAB_EPIC_MAPPER[jira_key] = response['iid']
  	print("Created gitlab Epic: %s" %response['iid'])
  	continue

  # Create GitLab issue
  print("Posting Jira issue %s to GitLab" %jira_key)
  gl_issue = gl_post_request('/projects/%s/issues' % GITLAB_PROJECT_ID, {
      'assignee_ids': [gl_assignee_id],
      'id': jira_key,
      'title': jira_title,
      'description': jira_description,
      'created_at': jira_created_at,
      'labels': ", ".join(gl_labels)
  })

  #Map JIRA issue Key with GitLab
  JIRA_GITLAB_ISSUE_MAPPER[jira_key] = gl_issue['iid']
  JIRA_GITLAB_iSSUE_ID_MAPPER[jira_key] = gl_issue['id']
  
  print("Start Importing attachment and comments for JIRA Issue key %s" %jira_key)
  issue_info = jira_get_request('/issue/%s/?fields=attachment,comment' % jira_key)

  for comment in issue_info['fields']['comment']['comments']:

  	if not comment:
  		continue

  	author = comment['author']['displayName']
  	created = comment['created']
  	updated = comment['updated']
  	description = comment['body']

  	print("Comment Author: %s" %author)
  	print("Comment Created Date: %s" %created)
  	print("Comment Updated Date: %s" %updated)
  	print("Comment: %s" %description)

  	#Post comments for the issue
  	print("Posting Comments")
  	added_note = gl_post_request('/projects/%s/issues/%s/notes' % (GITLAB_PROJECT_ID, gl_issue['iid']),{
  		'body':description,
  		'created_at':created
  		})

  #Extracting attachment Details
  for attachment in issue_info['fields']['attachment']:

  	if not attachment:
  		continue

  	fileName = attachment['filename']
  	mimeType = attachment['mimeType']
  	author = attachment['author']['displayName']

  	print("Attachemnt file :%s" %fileName)
  	print("Type: %s" %mimeType)
  	print("Author %s" %author)

  	#Download attachment
  	content = jira_downlaod_attachment(attachment['content'])

  	#Raise Exception if no file content was download
  	if content is None:
  		raise Exception("No Content in the attachment")

  	#Post Downloaded attachment to GitLab
  	print("Uploading file %s to Gitlab Project" %fileName)
  	file_info = gl_post_attachment('/projects/%s/uploads' %GITLAB_PROJECT_ID,{
  		'file':(
  			fileName,
  			content
  			)
  		})

  	#Posting a comment in GitLab with the attachment uploaded
  	note = gl_post_request('/projects/%s/issues/%s/notes' % (GITLAB_PROJECT_ID, gl_issue['iid']),{
  		'body':file_info['markdown'],
  		'created_at': attachment['created']
  		})

  	print("Successfully Created issue %s with comments and attachments in GitLab" %gl_issue['iid'])

  	#Change GitLab issue Status
  	if jira_issue_status == "Done":
  		print("Changin issue status to Close")
  		gl_put_request('/projects/%s/issues/%s' % (GITLAB_PROJECT_ID, gl_issue['iid']), {
      			'state_event': 'close'
    			})
  	else:
  		print(jira_issue_status)
    

  print("\n")


#################################################################################################################

#Iterate through JIRA_GITLAB_ISSUE_MAPPER TO CREATE LINKS BETWEEN ISSUES
for jira_issue_key in JIRA_GITLAB_ISSUE_MAPPER:
	jira_issue_details = jira_get_request('/issue/%s' %jira_issue_key)
	issuetype = jira_issue_details['fields']['issuetype']['name']

	if issuetype == 'Story' or issuetype == 'Bug':
		#Code to add Linkage with the epic
		print("This is a story")
		#Get Parent issue id
		if 'parent' in jira_issue_details['fields'] and jira_issue_details['fields']['parent'] is not None:
			parent_issue_id = jira_issue_details['fields']['parent']['key']
			#Create Link
			print("Adding Parent Link Connection to the epic")
			print("Jira Story ID %s" %jira_issue_key)
			print("Epic Id: %s" %JIRA_GIRLAB_EPIC_MAPPER[parent_issue_id]);
			print("Story id in GitLab: %s",JIRA_GITLAB_iSSUE_ID_MAPPER[jira_issue_key])
			response = gl_post_request('/groups/%s/epics/%s/issues/%s'%(GITLAB_GROUP_ID,JIRA_GIRLAB_EPIC_MAPPER[parent_issue_id],JIRA_GITLAB_iSSUE_ID_MAPPER[jira_issue_key]),{})
			print("Added Connection Successfully")

	if issuetype == 'Bug':
		#Adds the link between the bug and the Issue it's blocking
		issue_links_list = jira_issue_details['fields']['issuelinks']

		if issue_links_list:
			issue_link_dict = issue_links_list[0]

			if 'outwardIssue' in issue_link_dict:
				print("Adding Bug Outward links")
				parent_issue_id = issue_link_dict['outwardIssue']['key']
				response = gl_post_request('/projects/%s/issues/%s/links'%(GITLAB_PROJECT_ID,JIRA_GITLAB_ISSUE_MAPPER[jira_issue_key]),{
			        'target_project_id':GITLAB_PROJECT_ID,
			        'target_issue_iid':JIRA_GITLAB_ISSUE_MAPPER[parent_issue_id],
			        'link_type':'blocks'
			         })



	if issuetype == 'Subtask':
		#Get Parent issue id
		parent_issue_id = jira_issue_details['fields']['parent']['key']
		#Create Link
		print("Adding Parent Link Connection")
		response = gl_post_request('/projects/%s/issues/%s/links'%(GITLAB_PROJECT_ID,JIRA_GITLAB_ISSUE_MAPPER[jira_issue_key]),{
			'target_project_id':GITLAB_PROJECT_ID,
			'target_issue_iid':JIRA_GITLAB_ISSUE_MAPPER[parent_issue_id]
			})

	if issuetype == 'Task':

		issue_links_list = jira_issue_details['fields']['issuelinks']

		if issue_links_list:
			issue_link_dict = issue_links_list[0]
				
			#Check whether the link is a inward issue
			if 'inwardIssue' in issue_link_dict:
				print("Adding inwardIssue link for Task")
				parent_issue_id = issue_link_dict['inwardIssue']['key']
				response = gl_post_request('/projects/%s/issues/%s/links'%(GITLAB_PROJECT_ID,JIRA_GITLAB_ISSUE_MAPPER[jira_issue_key]),{
			        'target_project_id':GITLAB_PROJECT_ID,
			        'target_issue_iid':JIRA_GITLAB_ISSUE_MAPPER[parent_issue_id]
			         })
			#Check whether the link is a outward issue
			if 'outwardIssue' in issue_link_dict:
				print("Adding outwardIssue link for Task")
				parent_issue_id = issue_link_dict['outwardIssue']['key']
				response = gl_post_request('/projects/%s/issues/%s/links'%(GITLAB_PROJECT_ID,JIRA_GITLAB_ISSUE_MAPPER[jira_issue_key]),{
			        'target_project_id':GITLAB_PROJECT_ID,
			        'target_issue_iid':JIRA_GITLAB_ISSUE_MAPPER[parent_issue_id]
			         })

	print("\n")
